const mongoose = require('mongoose');
const Propietario = require('../models/Propietario');
const Auto = require('../models/Auto');
const { validationResult } = require('express-validator');

exports.crear = async (req, res) => {

    const errores = validationResult(req);
    if(!errores.isEmpty()){
        return res.status(412).json({ errors: errores.array() });
    }

    try {

        let propietario = new Propietario(req.body);

        await propietario.save();
        return res.status(201).json({ msg: 'Propietario creado correctamente.', propietario });

    } catch (error) {
        console.error(error);
        res.status(400).json({ msg: 'Hubo un error.' })
    }
};

exports.listar = async (req, res) => {
  try {
      const propietarios = await Propietario.find().sort('apellido');
      res.json({ propietarios });
  } catch (error) {
      console.error(error);
      res.status(400).json({ msg: 'Hubo un error.' })
  }
};


exports.crearAuto = async (req, res) => {

  const errores = validationResult(req);
  if(!errores.isEmpty()){
      return res.status(412).json({ errors: errores.array() });
  }

  try {

    // Validar el ID
    if(!mongoose.Types.ObjectId.isValid(req.params.id)){
      return res.status(404).json({ msg: 'El propietario no existe.' });
    }

      // Varificar que el propietario exista
      let propietario = await Propietario.findById(req.params.id);

      if(!propietario){
        return res.status(404).json({ msg: 'El propietario no existe.' });
      }

      req.body.propietario = propietario;

      let auto = new Auto(req.body);
      await auto.save();

      return res.status(201).json({ msg: 'Auto creado correctamente.', auto });

  } catch (error) {
      console.error(error);
      res.status(400).json({ msg: 'Hubo un error.' })
  }
};
