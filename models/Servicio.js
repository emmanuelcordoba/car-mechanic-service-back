const mongoose = require('mongoose');

const ServicioSchema = mongoose.Schema({
    nombre: {
        type: String,
        required: true,
        trim: true,
        unique: true
    },
    precio: {
        type: Number,
        required: true,
    }
})

module.exports = mongoose.model('Servicio',ServicioSchema);
