const mongoose = require('mongoose');

const AutoSchema = mongoose.Schema({
  marca: {
      type: String,
      required: true,
      trim: true
  },
  modelo: {
      type: String,
      required: true,
      trim: true
  },
  anio: {
      type: String,
      required: true,
      trim: true
  },
  patente: {
      type: String,
      required: true,
      trim: true,
      unique: true
  },
  color: {
    type: String,
    required: true,
    trim: true
  },
  propietario: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Propietario',
    required: true,
  },
  historial: [{
    servicios: [{
      nombre: {
        type: String,
        required: true,
        trim: true
      },
      precio: {
        type: Number,
        required: true,
      },
      _id: false,
    }],
    total: {
      type: Number,
      required: true,
    },
    fecha: {
      type: Date,
      default: Date.now()
    },
    _id: false,
  }]

})

module.exports = mongoose.model('Auto',AutoSchema);
