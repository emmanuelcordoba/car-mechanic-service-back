const mongoose = require('mongoose');

const PropietarioSchema = mongoose.Schema({
  apellido: {
      type: String,
      required: true,
      trim: true
  },
  nombre: {
      type: String,
      required: true,
      trim: true
  },
  documento: {
    type: String,
    required: true,
    trim: true,
    unique: true
  }
})

module.exports = mongoose.model('Propietario',PropietarioSchema);
