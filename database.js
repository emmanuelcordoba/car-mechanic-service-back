const mongoose = require('mongoose');

exports.connect = async () => {
  const URI = process.env.DB_MONGO;
  await mongoose.connect(URI)
  .then( () => console.log('Base de datos conectada.'))
  .catch( error => {
      console.error(error);
      process.exit(1); // Detiene el servidor
  });
};

exports.close = async () => {
  await mongoose.connection.close();
  console.log('Base de datos desconectada.');
}
